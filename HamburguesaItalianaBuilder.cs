﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Patrones.Builder.Core.Agregado;
using static Patrones.Builder.Core.Masa;
using static Patrones.Builder.Core.Salsa;

namespace Patrones.Builder.Core
    //la Hambueguesa italiana lleva los siguientes ingredientes el oregano ala piedra y salsa
{
      public class HamburguesaItalianaBuilder : HamburguesaBuilder
    {
        

        public HamburguesaItalianaBuilder()
        {
            _descripcion = "hambuerguesa italiana";
        }
          
            public override Agregado BuildAgregado()
        {
            return new Oregano();
        }
        public override Masa BuildMasa()
        {
            return new Alapiedra();

        }
        public override Salsa BuildSalsa()
        {
            return new Oliva();

        }

    }
}
