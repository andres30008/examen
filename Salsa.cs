﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Builder.Core
    //el tipo de salsa para la hamburguesa de tomate, olivia y light
{
    public abstract class Salsa
    {
        protected string _descripcion;
        public string Descripcion { get { return _descripcion; } }

        public object Description { get; internal set; }

        public class Tomate : Masa
        {
            public Tomate()
            {
                _descripcion = "Salsa de Tomate Normal";
            }
        }
        public class Oliva : Salsa
        {
            public Oliva()
            {
                _descripcion = "Salsa de olivia con toques de pimienta";
            }
        }
        public class Light : Salsa
        {
            public Light()
            {
                _descripcion = "Salsa para pelucones sin mucha sal";
            }
        }
    }
}
