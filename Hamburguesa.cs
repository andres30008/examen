﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Builder.Core
{
     public class Hamburguesa
        //esta es la clase principal donde declare masa, salsa y agregado para countrir una amburguesa con el patron builder
    {
        Masa _masa;
        Salsa _salsa;
        Agregado _agregado;
        string _tipo;
        public override string ToString()
        {
            return $"{_tipo}, Masa: {_masa.Description}, Salsa: {_salsa.Description}, Agregado: { _agregado.Description}";
        }
        public Hamburguesa(Masa masa, Salsa salsa, Agregado agregado, string tipo)
        {
            _masa = masa;
            _salsa = salsa;
            _agregado = agregado;
            _tipo = tipo;
        }
    }
}
