﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Builder.Core
    //este es el programa principal donde creo una clase abtracta con masa, salsa y agregado
{
     public  abstract class HamburguesaBuilder
    {
        protected string _descripcion;
        public abstract Masa BuildMasa();
        public abstract Salsa BuildSalsa();
        public abstract Agregado BuildAgregado();

        public override string ToString()
        {
            return _descripcion;
        }
        //la descripcion me va a retonar valores de masa salsa y agregado
        public Hamburguesa BuildHamburguesa()
        {
            Masa masa = BuildMasa();
            Salsa salsa = BuildSalsa();
            Agregado agregado = BuildAgregado();


          

            return new Hamburguesa(masa,salsa,agregado,_descripcion);


        }

    }
}
