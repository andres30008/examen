﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Builder.Core
    //se indica el tipo de masa para la Hamburguesa
{
    public abstract class Masa
    {
        protected string _descripcion;
        public string Descripcion { get { return _descripcion; } }

        public object Description { get; internal set; }

        public class Almode : Masa
        {
            public Almode()
            {
                _descripcion = "Masa para pan";
            }
        }
        public class Alapiedra : Masa
        {
            public Alapiedra()
            {
                _descripcion = "Masa para hornar pan en horno de piedra";
            }
        }
        public class Integral : Masa
        {
            public Integral()
            {
                _descripcion = "Masa para pan integral";
            }
        }
    }
}
