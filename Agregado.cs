﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Builder.Core
{
    public abstract class Agregado
        //la clase agragado tiene ingredientes extras para la hamburguesa oregano, huevo y carne
    {
        protected string _descripcion;
        public string Descripcion { get { return _descripcion; } }

        public object Description { get; internal set; }

        public class Oregano : Agregado
        {
            public Oregano()
            {
                _descripcion = "oregano con toques de pimienta";
            }
        }
        public class Huevo : Agregado
        {
            public Huevo()
            {
                _descripcion = "Huevo frito si para la hambuerguesa";
            }
        }
        public class Doblecarne : Agregado
        {
            public Doblecarne()
            {
                _descripcion = "Una racion extra de carnesita";
            }
        }
    }
}