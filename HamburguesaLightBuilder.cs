﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Patrones.Builder.Core.Agregado;
using static Patrones.Builder.Core.Masa;
using static Patrones.Builder.Core.Salsa;

namespace Patrones.Builder.Core
{
    //aquí se realizara la hamburguesa light que llevara ingrediente con menos grasa
    public class HamburguesaLightBuilder : HamburguesaBuilder
    {
        private string _description;

        public HamburguesaLightBuilder()
        {
            _description = "hambuerguesa light";
        }
        public override Agregado BuildAgregado()
        {
            return new Oregano();
        }
        public override Masa BuildMasa()
        {
            return new Integral();

        }
        public override Salsa BuildSalsa()
        {
            return new Light();

        }

    }
}